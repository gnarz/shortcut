#!/usr/bin/env bash

(
	cd test
	for t in *; do
		if [ -L $t ]; then rm $t; fi
	done
	rmdir directory
	rm invalid_executable
	rm not_executable
)

rm -rf zig-out zig-cache
