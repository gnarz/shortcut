#!/usr/bin/env bash

PWD=$(pwd)
PATH="$PWD/zig-out/bin":"$PATH":"$PWD/test"

# test and prepare executable
zig build

# prepare test directory
(
	cd test
	lnames=$(grep -E '^[a-z0-9_]+:' shortcuts | cut -d: -f1)
	for l in $lnames; do
		if [ \! -L $l ]; then ln -s shortcuts $l; fi
	done
	if [ \! -L test_undefined ]; then ln -s shortcuts test_undefined; fi
	mkdir -p directory
	echo "#! this is an invalid executable" > invalid_executable
	chmod +x invalid_executable
	echo "this file is not executable" > not_executable
)

# run tests
(
	cd test
	tests=$(grep -E '^test[a-z0-9_]+:' shortcuts | cut -d: -f1)
	for t in $tests; do
		if [ -L $l ]; then
			echo -n - "$t"
			s=`PATH=$PATH "$t" 2>&1`
			if [ $? -eq 0  ]; then echo -n " = true"; else echo -n " = false"; fi
			if [ -n "$s" ]; then echo " \"$s\""; else echo; fi
		fi
	done
)
