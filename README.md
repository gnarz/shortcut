# shortcut

a simple command shortcut facility for linux and probably other unixen. Kinda like shell alias, but portable between all shells.

## Build

build and install the program using this command:

	sudo zig build -Doptimize=ReleaseSafe --prefix /usr/local

## Usage

The program can be invoked like this:

	shortcut [-v|-h] shortcut args

However, the usual way is to create a shortcuts file, which is an executable file where the first line is a shebang line pointing to the shortcut program:

	#!/usr/bin/env shortcut

and containing entries like this:

	shortcut: expansion string

where shortcut is the name of the shortcut and expansion string is the actual command to execute. Then symlinks are created to this file whose name is the name of the shortcut to execute. See the Example section below.

If you want to debug your shortcuts, the shebang line should look like this:

	#!/usr/bin/env -S shortcut -v

With this, the expansions of the shortcuts will be printed to stderr, before they are executed.

### Command line flags

- -v verbose, output the shortcut expansion including all variable or parameter expansions to stderr before it is executed.
- -h display a short help

## Shortcut execution

When executing a shortcut, the first line to match from the shortcuts file is used. The shortcut expansion string is split at the whitespace into separate tokens. The first token is the name of the command to execute, and the remaining tokens are the arguments. Any part of the expansion string can be enclosed in double quotes to form a single token, even if it contains spaces. So with

	shotcut1: command with three arguments

command will have three arguments to the command, but with

	shortcut2: command "with only a single argument"

it will have only one.

The expansion string may contain references to parameters or environment parameters, which will be evaluated before the execution of the command. See below for more on this.

The result of this process will then be executed by the operating system as one command.

### Environment variables

Environment variables in shortcut expansions can be specified in 3 forms:

- \$VAR expands to the value of the environment variable VAR
- \${VAR} expands to the value of the environment variable VAR
- \${VAR|default} expands to the value of the environment variable VAR, or a default value if VAR is unset.

The second form us useful if he variable is directly adjacent to something that would look like a continuation of the variable name, for example something like

	somecut: echo ${VAR}_suffix

Undefined variables without a default value specified will be replaced by an empty string, and a warning will be given about this. If you don't want a warning, you can specify an empty default value as in \${VAR|}.

### Shortcut arguments

Shortcuts can have arguments, to which you can refer like so:

- \$n where n in 1..9 expands to argument #n
- \${n} where n in 1..9  expands to argument #n
- \${n|default} where n in 1..9 expands to argument #n or a default value if that is unset
- \$*n where n in 1..9  expands to argument #n and following
- \$* same as $*1

As can be seen here, only arguments 1-9 can be referred to explicitly, but the $* forms will expand to all arguments, regardless of how many there are.

The $n and ${n} forms work like the corresponding environment variable expansions, though the need for the second form is only for readability, as argument references may only ever have a single digit anyway. However, the $* forms must always be a separate token.

	ex1: echo this is fine: $*
	ex2: echo in the middle $*3 is also fine
	ex3: echo "in a string is not fine $*"
	ex4: echo directly adjacent$* to something $*else is also not fine
	ex5: echo quoting "$*2" is fine as long as it is alone within the quotes

Unset arguments without a default value specified will be replaced by an empty string, and a warning will be given about this. If you don't want a warning, you can specify an empty default value as in \${1|}. However, any of the \$* forms being empty is never considered being unset.

## Example

The shortcut file looks like this:

	#!/usr/bin/env shortcut

	Every line that does not start with a name followed by a colon is
	ignored and thus treated as a comment.
	
	# This means that you can choose any comment marker you like, or even
	# leave it our completely

	# a simple alias for ls -l
	ll: ls -lh $*

	# use zig as cc replacement
	cc: zig cc $*

	# swap the order of the first 2 arguments, and keep the rest as is. If
	# only one argument was given, echo that.
	revert: echo ${2|} $1 $*3

Then, within your PATH, create symbolic links to that file with the names ll, cc and revert, which will invoke the respective shortcuts.
