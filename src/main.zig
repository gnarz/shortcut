// shortcut
// by Gunnar Zötl <gz@tset.de>
// Released under the terms of the MIT license

const std = @import("std");

const VERSION = "0.4.3";

var debug: bool = false;

fn printError(prefix: []const u8, comptime msg: []const u8, args: anytype) void {
    const stderr = std.io.getStdErr().writer();
    stderr.print("{s}: ", .{prefix}) catch unreachable;
    stderr.print(msg, args) catch unreachable;
    stderr.print(".\n", .{}) catch unreachable;
}

fn showWarning(comptime msg: []const u8, args: anytype) void {
    printError("warning", msg, args);
}

fn raiseError(comptime msg: []const u8, args: anytype) noreturn {
    printError("error", msg, args);
    std.posix.exit(1);
}

fn readFile(fname: []const u8, allocator: std.mem.Allocator) ![]const u8 {
    var file = try std.fs.cwd().openFile(fname, .{ .mode = .read_only });
    const contents = try file.readToEndAlloc(allocator, std.math.maxInt(usize));
    return contents;
}

fn getShortcut(exe: []const u8, allcuts: []const u8) ?[]const u8 {
    const shortcut_name = std.fs.path.basename(exe);
    if (shortcut_name.len == 0) unreachable;
    var comparing: bool = true;
    var found: bool = false;
    var char: u32 = 0;
    var start: usize = 0;
    var end: usize = 0;
    for (allcuts, 0..) |c, i| {
        if (found) {
            if (c == '\n') break;
            end += 1;
        } else if (comparing and c != '\n') {
            if (char < shortcut_name.len and c == shortcut_name[char]) {
                char += 1;
            } else if (char == shortcut_name.len and c == ':') {
                found = true;
                end = i + 1;
                start = i + 1;
                comparing = false;
            } else {
                comparing = false;
            }
        } else if (c == '\n') {
            comparing = true;
            char = 0;
        }
    }
    return if (found) allcuts[start..end] else null;
}

fn isVarNameChar(c: u8) bool {
    return std.ascii.isAlphanumeric(c) or c == '_';
}

fn expand(str: []const u8, args: [][:0]const u8, allocator: std.mem.Allocator) ![]const u8 {
    var expanded = std.ArrayList(u8).init(allocator);
    var start: usize = 0;
    var c: u8 = 0;

    while (start < str.len) {
        c = str[start];
        if (c == '\\') {
            start += 1;
        } else if (c == '$') {
            start += 1;
            c = str[start];
            if (c == '{' or isVarNameChar(c)) {
                var s: []const u8 = undefined;
                var default: ?[]const u8 = null;
                if (c == '{') {
                    start = start + 1;
                    var end = start + 1;
                    while (end < str.len and str[end] != '}' and str[end] != '|') {
                        end += 1;
                    }
                    s = str[start..end];
                    if (str[end] == '|') {
                        start = end + 1;
                        end = start;
                        while (end < str.len and str[end] != '}') {
                            end += 1;
                        }
                        default = str[start..end];
                    }
                    start = end + 1;
                } else if (std.ascii.isDigit(c)) {
                    s = str[start .. start + 1];
                    start += 1;
                } else {
                    var end = start;
                    while (end < str.len and isVarNameChar(str[end])) {
                        end += 1;
                    }
                    s = str[start..end];
                    start = end;
                }
                if (s.len == 1 and std.ascii.isDigit(s[0])) {
                    const n = s[0] - '0';
                    if (s[0] > '0') {
                        if (n < args.len) {
                            try expanded.appendSlice(args[n]);
                        } else {
                            if (default == null) {
                                showWarning("parameter number {d} is empty", .{n});
                                default = "";
                            }
                            try expanded.appendSlice(default.?);
                        }
                        continue;
                    }
                } else if (s.len >= 1 and isVarNameChar(s[0]) and !std.ascii.isDigit(s[0])) {
                    const v = std.posix.getenv(s);
                    if (v == null and default == null) {
                        showWarning("environment variable {s} is empty", .{s});
                        default = "";
                    }
                    try expanded.appendSlice(v orelse default.?);
                    continue;
                }
            }
            return error.SyntaxError;
        } else {
            try expanded.append(c);
            start += 1;
        }
    }
    return expanded.items;
}

fn canExecute(file: []const u8) bool {
    std.posix.access(file, std.posix.X_OK) catch return false;
    return true;
}

fn invokeShortcut(shortcut: []const u8, args: [][:0]const u8, allocator: std.mem.Allocator) !void {
    var list = std.ArrayList([]const u8).init(allocator);
    var start: usize = 0;
    var end: usize = 0;
    var c: u8 = 0;

    while (start < shortcut.len and std.ascii.isWhitespace(shortcut[start])) start += 1;
    while (start < shortcut.len) {
        end = start;
        c = shortcut[start];
        if (c == '"') end += 1;
        while (end < shortcut.len) {
            if (c == '"') {
                if (shortcut[end] != '"') {
                    end += 1;
                } else break;
            } else if (!std.ascii.isWhitespace(shortcut[end])) {
                end += 1;
            } else break;
        }
        var s: []const u8 = undefined;
        if (c == '"') {
            s = shortcut[start + 1 .. end];
            end += 1;
        } else {
            s = shortcut[start..end];
        }
        if (s.len >= 2 and s[0] == '$' and s[1] == '*') {
            var n: usize = 1;
            if (s.len == 3 and std.ascii.isDigit(s[2]) and s[2] > '0') {
                n = s[2] - '0';
            } else if (s.len > 2) return error.SyntaxError;
            while (n < args.len) : (n += 1) {
                try list.append(args[n]);
            }
        } else {
            try list.append(try expand(s, args, allocator));
        }
        start = end;
        while (start < shortcut.len and std.ascii.isWhitespace(shortcut[start])) start += 1;
    }

    if (debug) {
        std.debug.print("{s}\n", .{list.items});
    }

    const e = std.process.execv(allocator, list.items);

    if (e == error.FileNotFound and canExecute(list.items[0])) return error.AccessDenied;
    return e;
}

fn usage(pgm: []const u8) noreturn {
    const stderr = std.io.getStdErr().writer();
    stderr.print("{s} {s}\n", .{ pgm, VERSION }) catch unreachable;
    stderr.print("usage: {s} [-v|-h] shortcut args\n", .{pgm}) catch unreachable;
    std.posix.exit(1);
}

pub fn main() void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var args = std.process.argsAlloc(allocator) catch |err| {
        raiseError("{s}", .{@errorName(err)});
    };

    var arg: usize = 1;
    while (arg < args.len and args[arg][0] == '-') {
        if (std.mem.eql(u8, "-v", args[arg])) {
            debug = true;
        } else if (std.mem.eql(u8, "-h", args[arg])) {
            usage(args[0]);
        }
        arg += 1;
    }

    if (args.len - arg < 1) {
        usage(args[0]);
    }

    const contents = readFile(args[arg], allocator) catch {
        raiseError("not a shortcut", .{});
    };

    const shortcut = getShortcut(args[arg], contents) orelse {
        raiseError("shortcut undefined", .{});
    };

    if (shortcut.len == 0) std.posix.exit(1);

    invokeShortcut(shortcut, args[arg..], allocator) catch |err| {
        switch (err) {
            error.SyntaxError => raiseError("shortcut expansion has a syntax error", .{}),
            error.AccessDenied => raiseError("shortcut target is not executable", .{}),
            error.FileNotFound => raiseError("shortcut target not found", .{}),
            else => raiseError("{s}", .{@errorName(err)}),
        }
    };
}
